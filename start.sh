#!/bin/bash

# Install dependencies listed in requirement.txt
pip install -r requirement.txt

# Run unit tests
python tests/unitests.py
