### OHCE

**Shell application**
- Say hello/good afternoon/good evening according to the time of day and in the user's language **(fr_FR and en_EN only)**
- Return the user's input in mirror
- If the user's input is a palindrome, say "Well said !"
- At the end, say hello/good afternoon/good evening according to the time of day and in the user's language


```
sh start.sh

============================= test session starts ==============================
collecting ... collected 6 items

unitests.py::OhceTests::test_en PASSED                                   [ 16%]
unitests.py::OhceTests::test_fr PASSED                                   [ 33%]
unitests.py::OhceTests::test_greeting PASSED                             [ 50%]
unitests.py::OhceTests::test_not_palindrom_french_morning PASSED         [ 66%]
unitests.py::OhceTests::test_palindrom_english_evening PASSED            [ 83%]
unitests.py::OhceTests::test_palindrome PASSED                           [100%]

============================== 6 passed in 0.01s ===============================
```