import datetime
import sys
import locale


class Greetings:
    well_said = {"fr_FR": "Bien dit !", "en_US": "Well said !"}
    hello = {"fr_FR": "Bonjour !", "en_US": "Hello !"}
    good_afternoon = {"fr_FR": "Bon après-midi !", "en_US": "Good afternoon !"}
    good_evening = {"fr_FR": "Bonsoir !", "en_US": "Good evening !"}

    def polite_shell(self, language, now):
        """
        :param language:
        :param now:

        - Say hello/good afternoon/good evening according to the time of day and in the user's language
        """
        if now.hour < 12:
            print(self.hello[language])
        elif now.hour < 18:
            print(self.good_afternoon[language])
        else:
            print(self.good_evening[language])


class Ohce(Greetings):
    def run(self, language=None, now=None, word=None):
        """
            *** Shell application ***
            - Say hello/good afternoon/good evening according to the time of day and in the user's language
            - Return the user's input in mirror
            - If the user's input is a palindrome, say "Well said !"
            - At the end, say hello/good afternoon/good evening according to the time of day and in the user's language
        """
        if not now:
            now = datetime.datetime.now()

        if not language:
            language = locale.getdefaultlocale()[0]

        self.polite_shell(language, now)
        if not word:
            word = input("Waiting for user input... \n")

        print(word[::-1])
        if word == word[::-1]:
            print(self.well_said[language])

        self.polite_shell(language, now)


if __name__ == "__main__":
    ohce = Ohce()
    ohce.run()
