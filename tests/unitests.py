import unittest
import sys
import locale
import datetime
from ohce import Ohce
import param
from io import StringIO


class OhceTests(unittest.TestCase):
    def setUp(self):
        self.ohce = Ohce()
        self.held, sys.stdout = sys.stdout, StringIO()

    def test_fr(self):
        """ The OHCE is writing in frnech if the language is french """

        # GIVEN french ohce
        self.ohce.run(param.fr, param.morning, 'test')
        output = sys.stdout.getvalue().strip()

        # SO ohce answer in french
        self.assertEqual(output.split("\n")[0], "Bonjour !")

    def test_en(self):
        """ The OHCE is writing in english if the language is english """
        # GIVEN english ohce
        self.ohce.run(param.en, param.morning, 'test')
        output = sys.stdout.getvalue().strip()

        # SO ohce answer in english
        self.assertEqual(output.split("\n")[0], "Hello !")

    def test_greeting(self):
        """ The OHCE always says 'Bonjour' then mirror the string """

        # GIVEN 8:00AM time and french ohce
        self.ohce.run(param.fr, param.morning, "test")
        output = sys.stdout.getvalue().strip()

        # SO ohce answer : 'Bonjour !'
        self.assertEqual(output.split("\n")[0], "Bonjour !")

    def test_palindrome(self):
        """ If it's a palindrom, then the OHCE say 'Bien dit !' """

        # GIVEN a palindrom and french ohce
        self.ohce.run(param.fr, param.morning, "racecar")
        output = sys.stdout.getvalue().strip()

        # OHCE answer : 'Bien dit !'
        self.assertEqual(output.split("\n")[2], "Bien dit !")

    def test_palindrom_english_evening(self):
        """ A palindrom, with english user and it's the evening """

        # GIVEN a case where the user is english, it's the evening and the user input is a palindrom
        self.ohce.run(param.en, param.evening, "racecar")
        output = sys.stdout.getvalue().strip()

        # OHCE answer should be : 'Good evening ! racecar Well said ! Good evening !'
        self.assertEqual(output, "Good evening !\nracecar\nWell said !\nGood evening !")

    def test_not_palindrom_french_morning(self):
        """ Not a palindrom, french user, and it's the morning """

        # GIVEN a case where the user is french, it's 8:00am and the input is not a palindrom
        self.ohce.run(param.fr, param.morning, "Je me sens pas bien :(")
        output = sys.stdout.getvalue().strip()

        # OHCE answer should be : 'Bonjour ! 'Je me sens pas bien :(' [::-1] Bonjour !'
        self.assertEqual(output, "Bonjour !\n(: neib sap snes em eJ\nBonjour !")

    def tearDown(self):
        sys.stdout = self.held


if __name__ == '__main__':
    unittest.main()
