import datetime

"""
    Various parameter to use in tests files
"""

morning = datetime.datetime(2023, 2, 13, 8, 0, 0)
afternoon = datetime.datetime(2023, 2, 13, 14, 0, 0)
evening = datetime.datetime(2023, 2, 13, 21, 0, 0)
fr = "fr_FR"
en = "en_US"
